/* /**Iteración #1: Arrows**

Crea una arrow function que tenga dos parametros a y b y
que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre
por consola la suma de los dos parametros.*/

/*1.1 Ejecuta esta función sin pasar ningún parametro*/

/* const arrowFunction = (a, b) => a + b;

const result = arrowFunction();
console.log(); */


/*1.2 Ejecuta esta función pasando un solo parametro*/

/* const arrowFunction = (a , b) => a + b;

const result = arrowFunction(10);
console.log(result); */



/*1.3 Ejecuta esta función pasando dos parametros
*/
/* const arrowFunction = (a , b) => a + b;

const result = arrowFunction(10,5);
console.log(result); */


//--------------------//

/**Iteración #2: Destructuring**/

/*
2.1 En base al siguiente javascript, crea variables en base a las propiedades
del objeto usando object destructuring e imprimelas por consola. Cuidado,
no hace falta hacer destructuring del array, solo del objeto.*/

//const game = {
//    title: 'The last us 2',
//    gender: ['action', 'zombie', 'survival'],
//    year: 2020
//}

//let {title} = game;
//console.log(game.title);

//let {gender} = game;
//console.log(game.gender);

//let {year} = game;
//console.log(game.year);


/*2.2 En base al siguiente javascript, usa destructuring para crear 3 variables
llamadas fruit1, fruit2 y fruit3, con los valores del array. Posteriormente
imprimelo por consola.*/

//const fruits = ['Banana', 'Strawberry', 'Orange'];
debugger

//let [ fruit1, fruit2, fruit3] = fruits;

//console.log(fruit1, fruit2, fruit3);

/*2.3 En base al siguiente javascript, usa destructuring para crear 2
variables igualandolo a la función e imprimiendolo por consola.*/

/* const animalFunction = () => {
    return {
       name: 'Bengal Tiger',
        race: 'Tiger',
    }
}

let animalObj = animalFunction();
//console.log(animalObj);

let {name: animalName} = animalObj;
let {race: animalRace} = animalObj;

console.log(animalName, animalRace); */

/*2.4 En base al siguiente javascript, usa destructuring para crear las
variables name y itv con sus respectivos valores. Posteriormente crea
3 variables usando igualmente el destructuring para cada uno de los años
y comprueba que todo esta bien imprimiendolo.*/

/* const car = {
    name: 'Mazda 6',
    itv: [2015, 2011, 2020],
}

let {name: carName} = car;
let {itv: carItv} = car;

console.log(carName, carItv);

let [ firstITV, secondITV, thidITV] = carItv;

console.log(firstITV, secondITV, thidITV); */


//------------------------------//


/**Iteración #3: Spread Operator**/

/* 3.1 Dado el siguiente array, crea una copia usando spread operators. */
/* const pointsList = [32, 54, 21, 64, 75, 43]

let pointListCopy = [...pointsList];
console.log(pointListCopy); */

/* 3.2 Dado el siguiente objeto, crea una copia usando spread operators.*/
/*  const toy1 = {
     name: 'Buzz lightyear',
     date: '20-30-1995',
     color: 'multicolor'
    };

let toyCopyObj = {...toy1};
console.log(toyCopyObj); */


/* 3.3 Dado los siguientes arrays, crea un nuevo array juntandolos usando
spread operatos. */
/* const pointsList = [32, 54, 21, 64, 75, 43];
const pointsLis2 = [54,87,99,65,32];

let newArray = [...pointsList, ...pointsLis2];
console.log(newArray); */

/* 3.4 Dado los siguientes objetos. Crea un nuevo objeto fusionando los dos
con spread operators. */
/* const toy2 = {
    name: 'Bus laiyiar',
    date: '20-30-1995',
    color: 'multicolor'
};

const toyUpdate = {
    lights: 'rgb',
    power: ['Volar like a dragon', 'MoonWalk']
};

let newToyObj = {...toy2,...toyUpdate};
console.log(newToyObj); */

/* 3.5 Dado el siguiente array. Crear una copia de él eliminando la posición 2
pero sin editar el array inicial. De nuevo, usando spread operatos. */
/* const colors = ['rojo', 'azul', 'amarillo', 'verde', 'naranja'];

let colorsCopy = [...colors];
console.log(colorsCopy);

colorsCopy.splice(1,1);
console.log(colorsCopy); */

//----------------------------------------//

/**Iteración #4: Map**/

/* 4.1 Dado el siguiente array, devuelve un array con sus nombres
utilizando .map(). */
const users = [
	{id:1, name: 'Abel'},
	{id:2, name: 'Julia'},
	{id:3, name: 'Pedro'},
	{id:4, name: 'Amanda'}
];

const usersNames = users.map(userName => userName.name);
console.log(usersNames);

/* 4.2 Dado el siguiente array, devuelve una lista que contenga los valores
de la propiedad .name y cambia el nombre a 'Anacleto' en caso de que
empiece por 'A'. */
const users = [
	{id:1, name: 'Abel'},
	{id:2, name: 'Julia'},
	{id:3, name: 'Pedro'},
	{id:4, name: 'Amanda'}
];

const usersNames = users.map(userName => userName.name);
console.log(usersNames);


/* 4.3 Dado el siguiente array, devuelve una lista que contenga los valores
de la propiedad .name y añade al valor de .name el string ' (Visitado)'
cuando el valor de la propiedad isVisited = true. */
const cities = [
	{isVisited:true, name: 'Tokyo'},
	{isVisited:false, name: 'Madagascar'},
	{isVisited:true, name: 'Amsterdam'},
	{isVisited:false, name: 'Seul'}
];

const citiesNames = cities.map(city => city.name);
console.log(citiesNames);

if ('isVisited'=== true) {
    cities.name = value + ' Visitado';
    console.log(citiesNames);
}
console.log(citiesNames);